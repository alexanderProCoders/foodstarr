$(function () {
$('.nav-sidebar a').each(function() {
    let location = window.location.protocol + '//' + window.location.host + window.location.pathname;
    let link = this.href;

    if (link === location) {
        console.log(true);
        $(this).addClass('active');
        $(this).closest('.parent-menu-item').addClass('menu-is-opening menu-open');
        $(this).closest('.nav-treeview').addClass('menu-open');
    }
});

    bsCustomFileInput.init();

    // Adding new ingredient
    $('#add-ingredient').on('click', function(e) {
        e.preventDefault();
        let row = '';
        if ($('.ingredients .ingredient').length) {
            row = $('.ingredients .ingredient').last().clone();
            row.find('.form-group').each(function(index) {
                let dataId = parseInt($(this).find('input').attr('data-id'));
                dataId++;
                let dataName = $(this).find('input').attr('data-name');
                $(this).find('input').val('');
                $(this).find('input').attr('data-id', dataId).attr('name', 'ingredients[' + dataId +'][' + dataName +']');
            })
            row.appendTo($('.ingredients'));
        } else {
            row =
                '<div class="ingredient mb-3">\n' +
                    '<div class="row">'+
                        '<div class="col-sm-5">' +
                            '<div class="form-group">\n' +
                                '<label>Name <sup style="color: red;">*</sup></label>\n' +
                                '<input data-id="1" data-name="title" type="text" name="ingredients[1][title]" class="form-control" placeholder="Enter Ingredient Title">\n' +
                            '</div>\n' +
                        '</div>' +
                        '<div class="col-sm-5">' +
                            '<div class="form-group">\n' +
                                '<label>Description <sup style="color: red;">*</sup></label>\n' +
                                '<input data-id="1" data-name="description" type="text" name="ingredients[1][description]" class="form-control" placeholder="Enter Ingredient Description">\n' +
                            '</div>\n' +
                        '</div>\n' +
                        '<div class="col-sm-2 d-flex align-items-center justify-content-center">' +
                            '<div class="form-group">\n' +
                                '<button type="button" class="delete-row" onclick="deleteRow($(this))">' +
                                    '<i class="fas fa-minus-circle"></i>'
                                '</button>'
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';
            $(row).appendTo($('.ingredients'));
        }

    })

    // Adding new step
    $('#add-step').on('click', function(e) {
        e.preventDefault();
        let row = '';
        if ($('.steps .step').length) {
            row = $('.steps .step').last().clone();
            row.find('.form-group').each(function() {
                let dataId = parseInt($(this).find('input').attr('data-id'));
                dataId++;
                let dataName = $(this).find('input').attr('data-name');
                $(this).find('input').val('');
                $(this).find('input').attr('data-id', dataId).attr('name', 'steps[' + dataId +'][' + dataName +']');
            })
            row.appendTo($('.steps'));
        } else {
            row =
                '<div class="step mb-3">\n' +
                    '<div class="row">\n' +
                        '<div class="col-sm-10">\n' +
                            '<div class="form-group">\n' +
                                '<label for="image-0">Image</label>\n' +
                                '<div class="input-group">\n' +
                                    '<div class="custom-file">\n' +
                                        '<input type="file" class="custom-file-input" name="steps[0][image]" id="image-0">\n' +
                                        '<label class="custom-file-label" for="image-0">Choose file</label>\n' +
                                    '</div>\n' +
                                    '<div class="input-group-append">\n' +
                                        '<span class="input-group-text">Upload</span>\n' +
                                    '</div>\n' +
                                '</div>\n' +
                            '</div>\n' +
                            '<div class="form-group">\n' +
                                '<label>Title <sup style="color: red;">*</sup></label>\n' +
                                '<input data-id="0" data-name="title" type="text" name="steps[0][title]" class="form-control" placeholder="Enter Step Title">\n' +
                            '</div>\n' +
                        '</div>\n' +
                        '<div class="col-sm-10">\n' +
                            '<div class="form-group">\n' +
                                '<label>Description <sup style="color: red;">*</sup></label>\n' +
                                '<textarea data-id="0" rows="3" data-name="description" name="steps[0][description]" class="form-control" placeholder="Enter Step Description">\n' +
                                '</textarea>\n' +
                            '</div>\n' +
                        '</div>\n' +
                        '<div class="col-sm-2 d-flex align-items-center justify-content-center">\n' +
                            '<div class="form-group">\n' +
                                '<button type="button" class="delete-row" onclick="deleteRow($(this))">\n' +
                                    '<i class="fas fa-minus-circle"></i>\n' +
                                '</button>\n' +
                            '</div>\n' +
                        '</div>\n' +
                    '</div>\n' +
                '</div>';
            $(row).appendTo($('.steps'));
        }
    })
});

function confirmDeletingImage() {
    if (confirm('Are you sure want to delete image?')) {
        $('.recipe-image').remove();
        $('.hiddenImageFile').removeClass('d-none').find('input[type=file]').val('');
    }
}

function deleteRow(that) {
    if (confirm('Are you sure want to delete row?')) {
        that.parents('.ingredient').remove();
    }
}

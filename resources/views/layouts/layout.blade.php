<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FoodStarr</title>
    <link rel="stylesheet" href="{{ asset('assets/front/css/style.css') }}?v=1" type="text/css" media="all"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,500,700%7CRoboto:400,500,700%7CHerr+Von+Muellerhoff:400,500,700%7CQuattrocento+Sans:400,500,700" type="text/css" media="all"/>
    @livewireStyles
</head>
<body class="home page page-template page-template-template-portfolio page-template-template-portfolio-php">
<div id="page">
    @auth
        <div class="auth-line">
            <div class="container ovh">
                <p class="f-left">Hello, {{ auth()->user()->name }}!</p>
                <p class="f-right">
                    <a href="{{ route('admin.index') }}" target="_blank">Admin panel</a> |
                    <a href="{{ route('logout', $post->slug) }}">Logout</a>
                </p>
            </div>
        </div>

    @endauth
    <div class="container">

        <header id="masthead" class="site-header">
            <div class="site-branding">
                <h1 class="site-title"><a href="{{ route('post.index', ['locale' => $locale]) }}" rel="home">FoodStarr</a></h1>
            </div>
            <nav id="site-navigation" class="main-navigation">
                <button class="menu-toggle">Menu</button>
                <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
                <div class="menu-menu-1-container">
                    <ul id="menu-menu-1" class="menu">
                        <li><a href="/">{{ __('menu.home') }}</a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- #masthead -->
        @yield('content')
        <!-- #content -->
    </div>
    <!-- .container -->
    <footer id="colophon" class="site-footer">
        <div class="container">
            <div class="site-info">
                <h1 style="font-family: 'Herr Von Muellerhoff';color: #ccc;font-weight:300;text-align: center;margin-bottom:0;margin-top:0;line-height:1.4;font-size: 46px;">Foodstarr</h1>
                <a target="blank" href="#">&copy; Foodsmart Inc. - All rights reserved.</a>
            </div>
        </div>
    </footer>
    <a href="#top" class="smoothup" title="Back to top"><span class="genericon genericon-collapse"></span></a>
</div>
<!-- #page -->
<script src="{{ asset('assets/front/js/script.js') }}"></script>
@livewireScripts
</body>
</html>

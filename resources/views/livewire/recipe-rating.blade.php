<div>
    <section class="rate-wrapper">
        <div>
        @auth
            <p>Rate this recipe</p>
            @if (session()->has('message'))
                <p>{{ session('message') }}</p>
            @endif
            @if($hideForm != true)
                <form class="wpcf7" wire:submit.prevent="rate()">
                    <div class="stars-holder">
                        <div class="stars">
                            <label for="star1">
                                <input hidden wire:model="rating" type="radio" id="star1" name="rating" value="1" />
                                <svg class="@if($rating >= 1 ) selected @else text-grey @endif " fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>
                            </label>
                            <label for="star2">
                                <input hidden wire:model="rating" type="radio" id="star2" name="rating" value="2" />
                                <svg class="@if($rating >= 2 ) selected @else text-grey @endif " fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>
                            </label>
                            <label for="star3">
                                <input hidden wire:model="rating" type="radio" id="star3" name="rating" value="3" />
                                <svg class="@if($rating >= 3 ) selected @else text-grey @endif " fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>
                            </label>
                            <label for="star4">
                                <input hidden wire:model="rating" type="radio" id="star4" name="rating" value="4" />
                                <svg class="@if($rating >= 4 ) selected @else text-grey @endif " fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>
                            </label>
                            <label for="star5">
                                <input hidden wire:model="rating" type="radio" id="star5" name="rating" value="5" />
                                <svg class="@if($rating >= 5 ) selected @else text-grey @endif " fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z"/></svg>
                            </label>
                        </div>
                        <div class="my-5">
                            @error('comment')
                            <p>{{ $message }}</p>
                            @enderror
                            <textarea wire:model.lazy="comment" name="description" placeholder="Comment.."></textarea>
                        </div>
                    </div>
                    <div class="block">
                        <button type="submit">Rate</button>
                        @auth
                            @if($currentId)
                                <button type="submit" wire:click.prevent="delete({{ $currentId }})" class="text-sm cursor-pointer">Delete</button>
                            @endif
                        @endauth
                    </div>
                </form>
            @endif
            @else
                <div>
                    <div class="">
                        You need to login in order to be able to rate the product!
                    </div>
                    <a href="/register">Register</a>
                </div>
            @endauth
        </div>
    </section>
    <section>
        <h2 >
            Ratings
        </h2>
        <div>
            @forelse ($comments as $comment)
                <div class="flex col-span-1">
                        <a href="{{ '@' . $comment->user->name }}">
                        </a>
                    <div class="relative px-4 mb-16 leading-6 text-left">
                            {{ $comment->comment }}
                        <div>
                            Rating: <strong>{{ $comment->rating }}</strong> ⭐
                            @auth
                                @if(auth()->user()->id == $comment->user_id || auth()->user()->role->name == 'admin' ))
                                - <a wire:click.prevent="delete({{ $comment->id }})" class="text-sm cursor-pointer">Delete</a>
                                @endif
                            @endauth
                        </div>
                        <div class="box-border text-left text-gray-700" style="quotes: auto;">
                            <a href="{{ '@' . $comment->user->username }}">
                                {{  $comment->user->name }}
                            </a>
                        </div>
                    </div>
                </div>
            @empty
                <div class="flex col-span-1">
                    No ratings
                </div>
            @endforelse

        </div>
    </section>

</div>

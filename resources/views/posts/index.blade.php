@extends('layouts.layout')
@section('content')
    <div id="content" class="site-content">

        <h1>{{ $locale }}</h1>
        <div id="primary" class="content-area column full">
            <main id="main" class="site-main">
                @if (count($posts))
                    <div class="grid portfoliogrid">
                        @foreach ($posts as $post)
                        <article class="hentry">
                            <header class="entry-header">
                                <div class="entry-thumbnail">
                                    <a href="{{ route('post.show', ['slug' => $post->slug, 'locale' => $locale]) }}">
                                        <img src="{{ asset($post->getImage()) }}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="{{ $post->title }}"/>
                                    </a>
                                </div>
                                <h2 class="entry-title"><a href="{{ route('post.show', ['slug' => $post->slug, 'locale' => $locale]) }}" rel="bookmark">{{ $post->title }}</a></h2>
                            </header>
                        </article>
                        @endforeach
                    </div>
                    <!-- .grid -->
                @endif
                {{ $posts->links() }}
                <br/>
            </main>
            <!-- #main -->
        </div>
        <!-- #primary -->
    </div>
@endsection

@extends('layouts.layout')
@section('content')
    <h1>{{ $locale }}</h1>
        <div id="content" class="site-content">
            <div id="primary" class="content-area column single-portfolio">
                <main id="main" class="site-main">

                    <article class="portfolio hentry">
                        <header class="entry-header">
                            <div class="entry-thumbnail">
                                <img width="800" height="533" src="{{ asset($post->getImage()) }}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="p1"/>
                            </div>
                            <h1 class="entry-title">{{ $post->title }}</h1>
                        </header>
                        <div class="entry-content">
                            @if (count($post->ingredients))
                                <h3>Ingredients</h3>
                                <table>
                                @foreach ($post->ingredients as $ingredient)
                                    <tr>
                                        <td>{{ $ingredient->title }}</td>
                                        <td>{{ $ingredient->description }}</td>
                                    </tr>
                                @endforeach
                                </table>
                            @endif
                            @if ($post->description)
                                <h3>Description</h3>
                                <p>{{ $post->description }}</p>
                            @endif
                        </div>
                        @livewire('recipe-rating', ['recipe' => $post], key($post->id))
                    </article>
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
        <!-- #content -->
@endsection

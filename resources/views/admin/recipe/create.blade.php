@extends('admin.layouts.layout')
@section('admin.layouts.content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>New Recipe</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                            <li class="breadcrumb-item active">New Recipe</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <div class="card card-primary">
            <!-- form start -->
            <form action="{{ route('recipes.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Title <sup style="color: red;">*</sup></label>
                        <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror" placeholder="Enter Recipe Title">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Description <sup style="color: red;">*</sup></label>
                        <textarea name="description" class="form-control @error('name') is-invalid @enderror" rows="3" placeholder="Enter Recipe Description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="file" id="image">
                                <label class="custom-file-label" for="image">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text">Upload</span>
                            </div>
                        </div>
                    </div>
                    <div class="ingredients">
                        <h3>Ingredients</h3>
                    </div>
                    <a class="btn btn-primary mb-3" id="add-ingredient" href="#">Add Ingredient</a>

                    <!-- TODO Will be implements on the next release -->
                    <div class="steps" style="display: none">
                        <h3>Steps</h3>
                    </div>
                    <a style="display: none" class="btn btn-primary mb-3" id="add-step" href="#">Add step</a>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.content -->
@endsection

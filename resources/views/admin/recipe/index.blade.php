@extends('admin.layouts.layout')
@section('admin.layouts.content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Recipes</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                            <li class="breadcrumb-item active">Recipes</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Title</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <a class="btn btn-primary mb-3" href="{{ route('recipes.create') }}">Add new Recipe</a>
                    @if (count($recipes))
                        <div class="mt-1">
                            <table class="table table-bordered text-nowrap table-hover">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th style="width: 150px;">Image</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Author</th>
                                        <th style="width: 30px">Actions</th>
                                    </tr>
                                </thead>
                            <tbody>
                            @foreach($recipes as $recipe)
                                <tr>
                                    <td>{{ $recipe->id }}</td>
                                    <td>
                                        <a href="{{ route('recipes.edit', ['recipe' => $recipe->id]) }}">
                                            @if ($recipe->image)
                                                <img width="150" src="{{ asset('storage/' . $recipe->image) }}" alt="Image for {{ $recipe->title }}">
                                            @else
                                                <img width="150" src="/assets/admin/img/default-150x150.png" alt="Image for {{ $recipe->title }}">
                                            @endif
                                        </a>
                                    </td>
                                    <td><a href="{{ route('recipes.edit', ['recipe' => $recipe->id]) }}">{{ $recipe->title }}</a></td>
                                    <td><p>{{ \Illuminate\Support\Str::limit($recipe->description, 50, $end='...') }}</p></td>
                                    <td><p>{{ $recipe->user->name }}</p></td>
                                    <td>
                                        <form action="{{ route('recipes.destroy', ['recipe' => $recipe->id]) }}" method="post" class="text-center">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure want to delete this recipe?');">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-1 clearfix">
                        {{ $recipes->links() }}
                    </div>
                    @else
                        <p class="text-center">No Recipes</p>
                    @endif
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    &copy Foodstarr 2021 All rights reserved
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
@endsection

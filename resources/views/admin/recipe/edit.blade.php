@extends('admin.layouts.layout')
@section('admin.layouts.content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Recipe</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
                        <li class="breadcrumb-item active">Blank Page</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Recipe "{{ $recipe->id }}"</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route('recipes.update', ['recipe' => $recipe->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Title</label>
                    <input type="text" name="title" value="{{ $recipe->title }}" class="form-control @error('name') is-invalid @enderror" placeholder="Enter Recipe Name">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                    <textarea name="description" class="form-control @error('name') is-invalid @enderror" rows="3" placeholder="Enter Recipe Description">{{ $recipe->description }}</textarea>
                </div>
                <div class="form-group">
                    @if ($recipe->image)
                        <div class="recipe-image">
                            <img class="recipe-image" width="150" src="{{ asset('storage/' . $recipe->image) }}" alt="Image for {{ $recipe->title }}">
                            <a href="#" onclick="confirmDeletingImage();">
                                <span class="badge badge-danger delete-badge">X</span>
                            </a>
                        </div>
                        <div class="hiddenImageFile d-none">
                            <label for="image">File input</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="image" value="{{ $recipe->image }}" id="image">
                                    <label class="custom-file-label" for="image">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                        </div>
                    @else
                        <label for="image">File input</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="image" value="{{ $recipe->image }}" id="image">
                                <label class="custom-file-label" for="image">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text">Upload</span>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="ingredients">
                    <h3>Ingredients</h3>
                    @if ($recipe->ingredients)
                        @foreach($recipe->ingredients as $ingredient)
                            <div class="ingredient mb-3">
                                <div class="row">
                                    <input type="hidden" name="ingredients[{{ $ingredient->id }}][id]" value="{{ $ingredient->id }}">
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label>Name <sup style="color: red;">*</sup></label>
                                            <input data-id="{{ $ingredient->id }}" data-name="title" value="{{ $ingredient->title }}" type="text" name="ingredients[{{ $ingredient->id }}][title]" class="form-control" placeholder="Enter Ingredient Title">
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label>Description <sup style="color: red;">*</sup></label>
                                            <input data-id="{{ $ingredient->id }}" value="{{ $ingredient->description }}" data-name="description" type="text" name="ingredients[{{ $ingredient->id }}][description]" class="form-control" placeholder="Enter Ingredient Description">
                                        </div>
                                    </div>
                                    <div class="col-sm-2 d-flex align-items-center justify-content-center">
                                        <div class="form-group">
                                            <button type="button" class="delete-row" onclick="deleteRow($(this))">
                                                <i class="fas fa-minus-circle"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <a class="btn btn-primary mb-3" id="add-ingredient" href="#">Add Ingredient</a>

                <!-- TODO Will be implements on the next release -->
                <div class="steps" style="display: none">
                    <h3>Steps</h3>
                    @if ($recipe->steps)
                        @foreach($recipe->steps as $step)
                            <div class="step mb-3">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <label for="image-0">Image</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="steps[0][image]" id="image-0">
                                                    <label class="custom-file-label" for="image-0">Choose file</label>
                                                </div>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">Upload</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Title <sup style="color: red;">*</sup></label>
                                            <input data-id="{{ $step->id }}" data-name="title" type="text" value="{{ $step->title }}" name="steps[{{ $step->id }}][title]" class="form-control" placeholder="Enter Step Title">
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <label>Description <sup style="color: red;">*</sup></label>
                                            <textarea data-id="{{ $step->id }}" rows="3" data-name="description" name="steps[{{ $step->id }}][description]" class="form-control" placeholder="Enter Step Description">{{ $step->description }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 d-flex align-items-center justify-content-center">
                                        <div class="form-group">
                                            <button type="button" class="delete-row" onclick="deleteRow($(this))">
                                                <i class="fas fa-minus-circle"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <a style="display: none" class="btn btn-primary mb-3" id="add-step" href="#">Add step</a>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
    <!-- /.content -->
@endsection

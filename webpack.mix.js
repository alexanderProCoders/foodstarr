const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'resources/assets/admin/plugins/fontawesome-free/css/all.min.css',
    'resources/assets/admin/css/adminlte.css',
    'resources/assets/admin/css/style.css'
], 'public/assets/admin/css/admin.css');

mix.styles([
    'resources/assets/front/css/woocommerce-layout.css',
    'resources/assets/front/css/woocommerce-smallscreen.css',
    'resources/assets/front/css/woocommerce.css',
    'resources/assets/front/css/font-awesome.min.css',
    'resources/assets/front/style.css',
    'resources/assets/front/css/easy-responsive-shortcodes.css',
], 'public/assets/front/css/style.css');

mix.scripts([
    'resources/assets/admin/plugins/jquery/jquery.min.js',
    'resources/assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js',
    'resources/assets/admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js',
    'resources/assets/admin/js/adminlte.min.js',
    'resources/assets/admin/js/admin.js'
], 'public/assets/admin/js/admin.js');

mix.scripts([
    'resources/assets/front/js/jquery.js',
    'resources/assets/front/js/plugins.js',
    'resources/assets/front/js/scripts.js',
    'resources/assets/front/js/masonry.pkgd.min.js',
], 'public/assets/front/js/script.js');

mix.copyDirectory('resources/assets/admin/plugins/fontawesome-free/webfonts', 'public/assets/admin/webfonts');
mix.copyDirectory('resources/assets/admin/img', 'public/assets/admin/img');
mix.copy('resources/assets/admin/css/adminlte.css.map', 'public/assets/admin/css/adminlte.css.map');

mix.copyDirectory('resources/assets/front/fonts', 'public/assets/front/fonts');

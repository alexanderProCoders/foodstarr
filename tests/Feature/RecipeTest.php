<?php

namespace Tests\Feature;

use App\Models\Recipe;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\CreatesApplication;
use Tests\Generators\RecipeGenerator;
use Tests\Generators\UserGenerator;
use Tests\TestCase;

class RecipeTest extends TestCase
{
    use CreatesApplication,
        DatabaseMigrations,
        RefreshDatabase,
        WithFaker;

    /**
     * @group Recipe4
     * @return void
     */
    public function testCreateRecipeStoreOnlyOneRecipe(): void
    {
        $user = UserGenerator::createAdminUser();
        $data = RecipeGenerator::generateData(['user_id' => $user->id]);

        $this->actingAs($user)
            ->post(route('recipes.store'), $data);

        $this->assertEquals(1, Recipe::all()->count());
    }

    /**
     * @group Recipe
     * @return void
     */
    public function testUserRoleAdminCanCreateRecipe(): void
    {
        $user = UserGenerator::createAdminUser();
        $data = RecipeGenerator::generateData(['user_id' => $user->id]);

        $this->actingAs($user)
            ->post(route('recipes.store'), $data)
            ->assertStatus(302)
            ->assertRedirect(route('recipes.index'));
    }

    /**
     * @group Recipe
     *
     * @return void
     */
    public function testUserRoleModeratorCanCreateRecipe(): void
    {
        $user = UserGenerator::createModeratorUser();
        $data = RecipeGenerator::generateData(['user_id' => $user->id]);

        $this->actingAs($user)
            ->post(route('recipes.store'), $data);

        $this->assertEquals(1, Recipe::all()->count());
    }


    /**
     * @group Recipe
     * @return void
     */
    public function testUserRoleAdminWontCreateRecipeWithoutTitle(): void
    {
        $user = UserGenerator::createAdminUser();
        $data = RecipeGenerator::generateData(['user_id' => $user->id]);

        unset($data['title']);

        $this->actingAs($user)
            ->post(route('recipes.store'), $data);

        $this->assertEquals(0, Recipe::all()->count());
    }

    /**
     * @group Recipe
     * @return void
     */
    public function testUserRoleAdminWontCreateRecipeWithoutDescription(): void
    {
        $user = UserGenerator::createAdminUser();
        $data = RecipeGenerator::generateData(['user_id' => $user->id]);

        unset($data['description']);

        $this->actingAs($user)
            ->post(route('recipes.store'), $data);

        $this->assertEquals(0, Recipe::all()->count());
    }

    /**
     * @group Recipe5
     * @return void
     */
    public function testUnauthenticatedUserWontCreateRecipe(): void
    {
        $data = RecipeGenerator::generateData();

        $this->post(route('recipes.store'), $data)->assertStatus(302)
        ->assertRedirect(route('login.create'));

        $this->assertDatabaseMissing('recipes', [
            'title' =>  $data['title']
        ]);
    }

    /**
     * @group cake
     * @return void
     */
    public function testCreateRecipeCakeAsModerator(): void
    {
        $user = UserGenerator::createModeratorUser();
        $data = RecipeGenerator::generateDataRecipeCake(['user_id' => $user->id]);

        $this->actingAs($user)
            ->post(route('recipes.store'), $data);

        $this->assertDatabaseHas('recipes', ['title' => 'Cake']);
    }

    /**
     * @group burger
     * @return void
     */
    public function testUpdateRecipeTitleToBurgerAsAdmin(): void
    {
        $user = UserGenerator::createAdminUser();
        $recipe = RecipeGenerator::createRecipe(['user_id' => $user->id]);
        $title = 'Burger';

        $this->actingAs($user)
            ->patch(route('recipes.update', ['recipe' => $recipe->id]), [
                'title' => $title,
                ])->assertStatus(302);

        $recipe->refresh();

        $this->assertEquals($recipe->title, $title);
    }

    /**
     * @group false
     * @return void
     */
    public function testWontUpdateRecipeWithExistingName(): void
    {
        $user = UserGenerator::createAdminUser();
        $recipe = RecipeGenerator::createRecipe(['user_id' => $user->id]);

        $recipeCakeData = RecipeGenerator::generateDataRecipeCake(['user_id' => $user->id]);
        $recipeCake = RecipeGenerator::createRecipe($recipeCakeData);

        $this->actingAs($user)
            ->patch(route('recipes.update', ['recipe' => $recipe->id]), [
                'title' => $recipeCake->title,
            ])->assertSessionHasErrors('title');

        $recipe->refresh();

        $this->assertNotEquals($recipe->title, $recipeCake->title);
    }

}

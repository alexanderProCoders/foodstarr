<?php

namespace Tests\Generators;

use App\Models\Recipe;

class RecipeGenerator
{

    public static function generateDataRecipeCake(array $data = []): array
    {
        $data = array_merge($data, [
            'title'   =>  'Cake',
        ]);

        return self::generateData($data);
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed
     */
    public static function createRecipe(array $data = [])
    {
        $generatedData = self::generateData($data);

        return Recipe::factory()->create($generatedData);
    }

    public  static function generateData(array $data = []): array
    {
        $recipeDefinition =  Recipe::factory()->definition();

        if (empty($data)) {
            return $recipeDefinition;
        }

        return  array_merge($recipeDefinition, $data);
    }
}

<?php

namespace Tests\Generators;

use App\Models\User;

class UserGenerator
{

    public static function createAdminUser(array $data = [])
    {
        return self::createUser(array_merge($data, [
            'is_admin' => User::USER_LEVEL_ADMIN
        ]))->first();
    }

    public static function createModeratorUser(array $data = [])
    {
        return self::createUser(array_merge($data, [
            'is_admin' => User::USER_LEVEL_MODERATOR
        ]))->first();
    }

    public static function createUser(array $data = [])
    {
        return User::factory($data)
            ->count(1)
            ->create();
    }
}

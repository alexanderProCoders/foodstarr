# FoodStarr

## Install

1. Get source code
```
git clone https://gitlab.com/alexanderProCoders/foodstarr.git

cd foodstarr
```

2. Prepare environment
```
cp .env.example .env
```

3. Install dependencies
```
composer install
```

4. Create Docker Images
```
docker-compose up -d --build
```

5. Docker Container /var/www/html

Run command locally
```
docker exec -it foodstarr_web_1 /bin/bash
```

6. Prepare DB schema
```
php artisan migrate:fresh
php artisan db:seed
```

7. SuperAdmin User
```
admin@gmail.com
password
```

## Environments
### Frontend part
http://localhost

### Login page
http://localhost/login

### Registration page
http://localhost/register

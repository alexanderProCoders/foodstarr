FROM richarvey/nginx-php-fpm:1.10.3

COPY . /app

WORKDIR /app

COPY --from=build-stage /app /var/www/html
COPY ./docker/config/nginx/default.conf /etc/nginx/sites-enabled/default.conf
COPY ./docker/config/php/php.ini /usr/local/etc/php/php.ini-development

WORKDIR /var/www/html

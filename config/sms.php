<?php

return [
    'sms_fly' => [
        'login'     =>  env('SMS_FLY_LOGIN', ''),
        'password'  =>  env('SMS_FLY_PASSWORD', ''),
    ],
    'smsc'  =>  [
        'api_key'   =>  env('SMSC_API_KEY', ''),
    ],

    'recipe_created' => [
        'name'  =>  'recipe'
    ],

    'service_phone' => '999999'
];

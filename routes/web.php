<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin', 'namespace' => 'App\Http\Controllers\Admin', 'middleware' => 'admin'], function () {
   Route::get('/', 'AdminController@index')->name('admin.index');
   Route::resource('/recipes', 'RecipeController');
});

Route::group(['middleware' => 'guest'], function () {
    Route::get('/register',[UserController::class, 'create'])->name('register.create');
    Route::post('/register',[UserController::class, 'store'])->name('register.store');
    Route::get('/login',[UserController::class, 'loginForm'])->name('login.create');
    Route::post('/login',[UserController::class, 'login'])->name('login');
});

Route::get('/logout',[UserController::class, 'logout'])->name('logout')->middleware('auth');

Route::group(['middleware' => ['localize', 'ShareCommonData']], function () {
    Route::group(['prefix' => '{locale}'], function () {
        Route::get('/', [PostController::class, 'index'])->name('post.index');
        Route::get('/post/{slug}', [PostController::class, 'show'])->name('post.show');
    });
});





<?php

namespace App\Providers;

use App\Services\Image\ImageService;
use App\Services\Image\ImageServiceInterface;
use Illuminate\Support\ServiceProvider;

class ImageServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            ImageServiceInterface::class,
            ImageService::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

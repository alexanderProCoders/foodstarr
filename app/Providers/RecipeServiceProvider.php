<?php

namespace App\Providers;

use App\Services\Recipe\RecipeService;
use App\Services\Recipe\RecipeServiceInterface;
use App\Services\Recipe\Repositories\RecipeRepository;
use App\Services\Recipe\Repositories\RecipeRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RecipeServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            RecipeServiceInterface::class,
            RecipeService::class
        );

        $this->app->bind(
        RecipeRepositoryInterface::class,
        RecipeRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

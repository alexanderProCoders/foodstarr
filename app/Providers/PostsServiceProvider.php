<?php

namespace App\Providers;

use App\Services\Post\Repositories\PostsRepository;
use App\Services\Post\Repositories\PostsRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class PostsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            PostsRepositoryInterface::class,
            PostsRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootBindingEvents();
    }

    private function bootBindingEvents(): void
    {
        $this->app->resolving(PostsRepository::class, function (PostsRepository $postsRepository) {
            \Log::info(PostsRepository::class . " was created");
        });
    }
}

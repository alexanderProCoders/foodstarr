<?php

namespace App\Providers;

use App\Services\Step\StepService;
use App\Services\Step\StepServiceInterface;
use Illuminate\Support\ServiceProvider;

class StepServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            StepServiceInterface::class,
            StepService::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

namespace App\Providers;

use App\Http\Controllers\PostController;
use App\Services\SMS\Providers\FlySMSProvider;
use App\Services\SMS\Providers\SMSProviderInterface;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
        Paginator::useBootstrap();

        $this->registerSMSBindings();
        $this->bootBindingsEvent();
    }

    private function bootBindingsEvent()
    {
        $this->app->resolving(PostController::class, function (PostController $postController) {
            Log::info('Created PostController');
        });
    }

    private function registerSMSBindings()
    {
        $flySMSProvider = new FlySMSProvider(
            config('sms.sms_fly.login'),
            config('sms.sms_fly.password')
        );

        $this->app->instance(FlySMSProvider::class, $flySMSProvider);


        $this->app->bind(
            SMSProviderInterface::class,
            FlySMSProvider::class
        );
    }
}

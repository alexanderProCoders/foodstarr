<?php

namespace App\Providers;

use App\Services\SMS\Providers\SMSCProvider;
use App\Services\SMS\Providers\SMSProviderInterface;
use App\Services\SMS\SMSService;
use App\Services\SMS\SMSServiceInterface;
use Illuminate\Support\ServiceProvider;

class SMSServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            SMSServiceInterface::class,
            SMSService::class
        );

        $this->app->bind(
            SMSProviderInterface::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

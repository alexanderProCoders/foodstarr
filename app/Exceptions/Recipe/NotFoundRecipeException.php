<?php

namespace App\Exceptions\Recipe;

use Exception;
use Throwable;

class NotFoundRecipeException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);


    }
}

<?php

namespace App\Services\Post\Repositories;

use App\Models\Recipe;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;


class PostsRepository implements PostsRepositoryInterface
{
    public function search(array $filters = []): LengthAwarePaginator
    {
        $recipe = Recipe::query();
        $this->applyFilters($recipe, $filters);

        return $recipe->paginate(config('posts.posts_per_page'));
    }

    public function findPostBySlug(string $slug): ?Recipe
    {
        return Recipe::where('slug', $slug)->firstOrFail();
    }

    private function applyFilters(Builder $qb, array $filters): void
    {
        if (isset($filters['name'])) {
            $qb->where('name', $filters['name']);
        }

        $qb->orderBy('created_at', 'desc');
    }
}

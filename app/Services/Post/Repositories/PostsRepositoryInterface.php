<?php

namespace App\Services\Post\Repositories;

use App\Models\Recipe;
use Illuminate\Pagination\LengthAwarePaginator;

interface PostsRepositoryInterface
{

    public function search(): LengthAwarePaginator;

    public function findPostBySlug(string $slug): ?Recipe;
}

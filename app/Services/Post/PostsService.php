<?php

namespace App\Services\Post;

use App\Models\Recipe;
use App\Services\Post\Repositories\PostsRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;

class PostsService
{


    private PostsRepositoryInterface $postsRepository;

    public function __construct(PostsRepositoryInterface $postsRepository)
    {
        $this->postsRepository = $postsRepository;
    }


    public function searchPosts(): LengthAwarePaginator
    {
        return $this->postsRepository->search();
    }

    public function showPostBySlug(string $slug): ?Recipe
    {
        return $this->postsRepository->findPostBySlug($slug);
    }
}

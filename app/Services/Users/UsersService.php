<?php

namespace App\Services\Users;

use App\Models\User;
use App\Services\Users\Handlers\CreateUserHandler;
use App\Services\Users\Repositories\UsersRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class UsersService
{
    private CreateUserHandler $createUserHandler;

    public function __construct(
        CreateUserHandler $createUserHandler
    )
    {
        $this->createUserHandler = $createUserHandler;
    }

    public function storeUser(array $data): ?User
    {
        return $this->createUserHandler->handle($data);
    }

    public function login(User $user): void
    {

        Auth::login($user);
    }

    public function loginByCredentials(array $data): bool
    {
        return Auth::attempt($data);
    }

    public function logout(): void
    {
        Auth::logout();
    }
}

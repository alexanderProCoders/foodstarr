<?php

namespace App\Services\Users\Repositories;

interface UsersRepositoryInterface
{
    public function createFromArray(array $data);

}

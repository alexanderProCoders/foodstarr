<?php

namespace App\Services\Users\Repositories;

use App\Models\User;

class UsersRepository implements UsersRepositoryInterface
{

    public function createFromArray(array $data) :?User
    {
        return User::create($data);
    }
}

<?php

namespace App\Services\Users\Handlers;

use App\Models\User;
use App\Services\Users\Repositories\UsersRepositoryInterface;

class CreateUserHandler
{
    private UsersRepositoryInterface $usersRepository;

    public function __construct(UsersRepositoryInterface $usersRepository)
    {

        $this->usersRepository = $usersRepository;
    }

    public function handle(array $data): ?User
    {
        $data['password'] = bcrypt($data['password']);

        return $this->usersRepository->createFromArray($data);
    }
}

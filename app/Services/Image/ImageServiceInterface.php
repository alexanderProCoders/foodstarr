<?php


namespace App\Services\Image;


use App\Http\Requests\RecipeRequest;
use App\Models\Recipe;

interface ImageServiceInterface
{
    public const REQUEST_FILE_KEY = 'file';
    public const IMAGE_FOLDER = 'images';

    /**
     * @param RecipeRequest $request
     *
     * @return string|null
     */
    public function uploadRecipeImage(RecipeRequest $request): ?string;

    public function deleteRecipeImage(Recipe $recipe): void;

    public function uploadStepImage(RecipeRequest $request);
}

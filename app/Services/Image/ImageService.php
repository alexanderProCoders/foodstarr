<?php


namespace App\Services\Image;


use App\Http\Requests\RecipeRequest;
use App\Models\Recipe;
use Illuminate\Support\Facades\Storage;

class ImageService implements ImageServiceInterface
{
    private string $folder;

    public function __construct()
    {
        $this->folder = date('Y-m-d');
    }

    /**
     * @param RecipeRequest $request
     *
     * @return string|null
     */
    public function uploadRecipeImage(RecipeRequest $request): ?string
    {
        if (is_null($request->file(self::REQUEST_FILE_KEY))) {
            return null;
        }

        return $request->file(self::REQUEST_FILE_KEY)->store(self::IMAGE_FOLDER ."/{$this->folder}");
    }

    public function deleteRecipeImage(Recipe $recipe): void
    {
        Storage::delete($recipe->image);
    }

    public function uploadStepImage(RecipeRequest $request)
    {
        if (is_null($request->file(self::REQUEST_FILE_KEY))) {
            return null;
        }

        return $request->file(self::REQUEST_FILE_KEY)->store(self::IMAGE_FOLDER . "/{$this->folder}");
    }
}

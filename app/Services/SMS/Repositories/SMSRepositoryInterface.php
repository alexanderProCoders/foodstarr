<?php

namespace App\Services\SMS\Repositories;

interface SMSRepositoryInterface
{

    public function store(array $data);

    public function update(string $id, array $data);

    public function find(string $id);
}

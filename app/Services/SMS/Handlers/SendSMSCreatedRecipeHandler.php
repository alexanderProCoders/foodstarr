<?php

namespace App\Services\SMS\Handlers;

use App\Models\Recipe;
use App\Services\SMS\SMSService;

class SendSMSCreatedRecipeHandler
{

    private SMSService $SMSService;

    public function __construct(
        SMSService $SMSService
    )
    {

        $this->SMSService = $SMSService;
    }

    public function handle(Recipe $recipe)
    {
        $this->SMSService->sendSMS(
            $this->getSendToPhone(),
            $this->generateSMSMessage($recipe)
        );
    }

    private function getSendToPhone(): string
    {
        return config('sms.service_phone');
    }

    private function generateSMSMessage(Recipe $recipe): string
    {
        return trans('sms.recipe_created', [
                'name'  =>  $recipe->title
            ]);
    }
}

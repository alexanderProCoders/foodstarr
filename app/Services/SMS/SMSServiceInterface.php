<?php

namespace App\Services\SMS;

interface SMSServiceInterface
{

    public function sendSMS(string $phone, string $message);

    public function getSMSStatus(string $id): string;

    public function getAccountBalance(): int;
}

<?php

namespace App\Services\SMS\DTO;

class SentSMSDTO
{

    private string $id;
    private string $phone;
    private string $message;

    private function __construct(
        string $id,
        string $phone,
        string $message
    )
    {
        $this->message = $message;
        $this->id = $id;
        $this->phone = $phone;
    }

    /**
     * @param array $data
     *
     * @return SentSMSDTO
     */
    public static function fromArray(array $data): SentSMSDTO
    {
        return new self(
            $data['id'],
            $data['phone'],
            $data['message']
        );
    }
}

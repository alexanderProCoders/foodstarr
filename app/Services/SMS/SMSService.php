<?php

namespace App\Services\SMS;

use App\Services\SMS\Providers\SMSProviderInterface;

class SMSService implements SMSServiceInterface
{
    /**
     * @var SMSProviderInterface
     */
    private SMSProviderInterface $SMSProvider;

    public function __construct(SMSProviderInterface $SMSProvider)
    {
        $this->SMSProvider = $SMSProvider;
    }

    public function sendSMS(string $phone, string $message)
    {
        $smsDTO = $this->SMSProvider->send($phone, $message);
        // save sms
    }

    public function getSMSStatus(string $id): string
    {
        return $this->SMSProvider->status($id);
    }

    public function getAccountBalance(): int
    {
        return $this->SMSProvider->balance();
    }
}

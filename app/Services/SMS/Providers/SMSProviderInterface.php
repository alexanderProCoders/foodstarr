<?php

namespace App\Services\SMS\Providers;

use App\Services\SMS\DTO\SentSMSDTO;

interface SMSProviderInterface
{

    /**
     * @param string $phone
     * @param string $message
     * @return mixed
     */
    public function send(string $phone, string $message): SentSMSDTO;


    /**
     * @param string $id
     *
     * @return string
     */
    public function status(string $id): string;

    /**
     * @return int
     */
    public function balance(): int;

}

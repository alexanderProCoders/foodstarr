<?php

namespace App\Services\SMS\Providers;

use App\Services\SMS\DTO\SentSMSDTO;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;


class SMSCProvider implements SMSProviderInterface
{

    private string $apiToken;

    public function __construct(string $apiToken)
    {
        $this->apiToken = $apiToken;
    }

    /**
     * @param string $phone
     * @param string $message
     *
     * @return mixed|void
     */
    public function send(string $phone, string $message): SentSMSDTO
    {
        Log::info('SMS send',[
            'provider'  =>  self::class,
            'phone'     =>  $phone,
            'message'   =>  $message
        ]);

        return SentSMSDTO::fromArray([
            'id'        =>  Str::random(16),
            'phone'     =>  $phone,
            'message'   =>  $message,
        ]);
    }


    /**
     * @param string $id
     *
     * @return string
     */
    public function status(string $id): string
    {
        Log::info('SMS status', [
            'provider'  =>  self::class,
            'id'        =>  $id,
        ]);

        return $id;
    }

    /**
     * @return int|void
     */
    public function balance(): int
    {
        Log::info('balance', [
            'provider'  =>  self::class
        ]);
        return 1;
    }
}

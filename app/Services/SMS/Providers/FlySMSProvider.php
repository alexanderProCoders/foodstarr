<?php

namespace App\Services\SMS\Providers;

use App\Services\SMS\DTO\SentSMSDTO;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class FlySMSProvider implements SMSProviderInterface
{

    private string $login;
    private string $password;

    public function __construct(string $login, string $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * @inheritDoc
     */
    public function send(string $phone, string $message): SentSMSDTO
    {
        Log::info('SMS send',[
            'provider'  =>  self::class,
            'phone'     =>  $phone,
            'message'   =>  $message
        ]);

        return SentSMSDTO::fromArray([
            'id'    =>  Str::random(16),
            'phone'    =>  $phone,
            'message'    =>  $message,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function status(string $id): string
    {
        Log::info('SMS status', [
            'provider'  =>  self::class,
            'id'        =>  $id,
        ]);

        return $id;
    }

    public function balance(): int
    {
        Log::info('balance', [
            'provider'  =>  self::class
        ]);
        return 1;
    }
}

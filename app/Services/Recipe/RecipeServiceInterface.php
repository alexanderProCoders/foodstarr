<?php


namespace App\Services\Recipe;


use App\Http\Requests\RecipeRequest;
use App\Models\Recipe;
use Illuminate\Pagination\LengthAwarePaginator;

interface RecipeServiceInterface
{
    public const RECIPES_PER_PAGE = 5;

    /**
     * @return LengthAwarePaginator|null
     */
    public function getPaginatedRecipes(): ?LengthAwarePaginator;

    /**
     * @param RecipeRequest $request
     *
     * @return Recipe
     */
    public function createRecipe(RecipeRequest $request): Recipe;

    /**
     * @param int $id
     *
     * @return Recipe
     */
    public function getRecipeById(int $id): Recipe;

    /**
     * @param RecipeRequest $request
     * @param int $id
     */
    public function updateRecipe(RecipeRequest $request, int $id): void;

    /**
     * @param int $id
     *
     * @return Recipe
     */
    public function deleteRecipe(int $id): Recipe;
}

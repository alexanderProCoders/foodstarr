<?php

namespace App\Services\Recipe\Repositories;


interface RecipeRepositoryInterface
{

    public function findRecipeById(int $id);

    public function createRecipe(array $data);
}

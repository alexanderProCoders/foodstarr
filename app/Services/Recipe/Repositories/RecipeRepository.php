<?php

namespace App\Services\Recipe\Repositories;


use App\Models\Recipe;
use Exception;
use RuntimeException;


class RecipeRepository implements RecipeRepositoryInterface
{

    public function findRecipeById(int $id)
    {
        return Recipe::find($id);
    }

    /**
     * @throws Exception
     */
    public function createRecipe(array $data): ?Recipe
    {
        try {
            return Recipe::create($data);
        } catch (Exception $e) {
            throw new RuntimeException($e->getMessage());
        }
    }
}

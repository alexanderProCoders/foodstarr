<?php

namespace App\Services\Recipe\Handlers;

use App\Services\Recipe\Repositories\RecipeRepository;
use App\Services\SMS\Handlers\SendSMSCreatedRecipeHandler;
use Illuminate\Support\Facades\Auth;

class CreateRecipeHandler
{

    private RecipeRepository $recipeRepository;
    private SendSMSCreatedRecipeHandler $sendSMSCreatedRecipeHandler;

    public function __construct(
        RecipeRepository $recipeRepository,
        SendSMSCreatedRecipeHandler $sendSMSCreatedRecipeHandler
    )
    {
        $this->recipeRepository = $recipeRepository;
        $this->sendSMSCreatedRecipeHandler = $sendSMSCreatedRecipeHandler;
    }

    public function handle(array $data)
    {
        $data['title'] = ucfirst($data['title']);
        $data['user_id'] = Auth::user()->id;

        $recipe = $this->recipeRepository->createRecipe($data);

        $this->sendSMSCreatedRecipeHandler->handle($recipe);

        return $recipe;
    }
}

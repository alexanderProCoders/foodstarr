<?php

namespace App\Services\Recipe;


use App\Http\Requests\RecipeRequest;
use App\Models\Recipe;
use App\Services\Image\ImageServiceInterface;
use App\Services\Recipe\Handlers\CreateRecipeHandler;
use App\Services\Recipe\Repositories\RecipeRepositoryInterface;
use App\Services\Step\StepServiceInterface;
use App\Services\Ingredient\IngredientServiceInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class RecipeService implements RecipeServiceInterface
{
    private ImageServiceInterface $imageService;
    private IngredientServiceInterface $ingredientService;
    private StepServiceInterface $stepService;
    private RecipeRepositoryInterface $recipeRepository;
    private CreateRecipeHandler $createRecipeHandler;

    /**
     * RecipeService constructor.
     * @param ImageServiceInterface $imageService
     * @param IngredientServiceInterface $ingredientService
     * @param StepServiceInterface $stepService
     * @param RecipeRepositoryInterface $recipeRepository
     */
    public function __construct(
        ImageServiceInterface $imageService,
        IngredientServiceInterface $ingredientService,
        StepServiceInterface $stepService,
        RecipeRepositoryInterface $recipeRepository,
        CreateRecipeHandler $createRecipeHandler
    )
    {
        $this->imageService = $imageService;
        $this->ingredientService = $ingredientService;
        $this->stepService = $stepService;
        $this->recipeRepository = $recipeRepository;
        $this->createRecipeHandler = $createRecipeHandler;
    }

    /**
     * @return LengthAwarePaginator|null
     */
    public function getPaginatedRecipes(): ?LengthAwarePaginator
    {
        if (Auth::user()->is_admin) {
            return Recipe::orderBy('updated_at', 'desc')
                ->paginate(self::RECIPES_PER_PAGE);
        }

        return Recipe::where('user_id', Auth::user()->id)
            ->orderBy('updated_at', 'desc')
            ->paginate(self::RECIPES_PER_PAGE);

    }

    /**
     * @param RecipeRequest $request
     *
     * @return Recipe
     */
    public function createRecipe(RecipeRequest $request): Recipe
    {
        $image = $this->imageService->uploadRecipeImage($request);

        $request->request->add(['image' => $image]);

        $recipe = $this->createRecipeHandler->handle($request->all());

        if (is_null($recipe) === false) {
            $this->ingredientService->createIngredients($request, $recipe);
        }

        return $recipe;
    }

    /**
     * @param int $id
     *
     * @return Recipe
     * @throws \Exception
     */
    public function getRecipeById(int $id): Recipe
    {
        $recipe = $this->recipeRepository->findRecipeById($id);

        if (is_null($recipe)) {
            throw new \Exception('Recipe doesnt exists.');
        }

        return $recipe;
    }

    /**
     * @param RecipeRequest $request
     * @param int $id
     */
    public function updateRecipe(RecipeRequest $request, int $id): void
    {
        $recipe = $this->getRecipeById($id);

        $data = $request->all();

        if ($file = $this->uploadOrDeleteRecipeImage($request, $recipe)) {
            $data['image'] = $file;
        }

        $recipe->update($data);

        $this->ingredientService->deleteIngredients($request, $recipe);
        $this->ingredientService->editIngredients($request, $recipe);
    }

    /**
     * @param int $id
     * @return Recipe
     */
    public function deleteRecipe(int $id): Recipe
    {
        $recipe = $this->getRecipeById($id);
        $recipe->ingredients()->where('recipe_id', $id)->delete();
        $recipe->delete($id);

        return $recipe;
    }

    /**
     * @param RecipeRequest $request
     * @param Recipe $recipe
     *
     * @return string|null
     */
    protected function uploadOrDeleteRecipeImage(RecipeRequest $request, Recipe $recipe): ?string
    {
        if (is_null($request->image)) {
            return null;
        }

        if ($recipe->image !== $request->image) {
            if (!is_null($recipe->image)) {
                $this->imageService->deleteRecipeImage($recipe);
            }

            return $this->imageService->uploadRecipeImage($request);
        }

        return null;
    }
}

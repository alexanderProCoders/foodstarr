<?php


namespace App\Services\Step;


use Illuminate\Database\Eloquent\Collection;

interface StepServiceInterface
{

    /**
     * @param $request
     * @param $recipe
     *
     * @return Collection|null
     */
    public function createSteps($request, $recipe): ?Collection;
}

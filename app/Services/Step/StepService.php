<?php


namespace App\Services\Step;


use App\Models\Recipe;
use App\Services\Image\ImageServiceInterface;
use Illuminate\Database\Eloquent\Collection;

class StepService implements StepServiceInterface
{

    private ImageServiceInterface $imageService;

    public function __construct(ImageServiceInterface $imageService)
    {
        $this->imageService = $imageService;
    }
    /**
     * @param $request
     * @param $recipe
     *
     * @return Collection|null
     */
    public function createSteps($request, $recipe): ?Collection
    {
        if (is_null($request->steps)) {
            return null;
        }
        $images = $this->imageService->uploadStepImage($request);
        try {
            foreach ($request->steps as $step) {

                $recipe->steps()->create([
                    'title'         => $step->title,
                    'description'   =>  $step->description,
                    'image'         =>  $image,
                ]);
            }


        } catch(\Exception $ex){
            dd($ex);
        }

        return $recipe->ingredients()->createMany($request->ingredients);
    }
}

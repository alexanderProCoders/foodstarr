<?php


namespace App\Services\Ingredient;

use App\Http\Requests\RecipeRequest;
use App\Models\Recipe;
use Illuminate\Database\Eloquent\Collection;

class IngredientService implements IngredientServiceInterface
{

    /**
     * @param RecipeRequest $request
     * @param Recipe $recipe
     *
     * @return Collection|void
     */
    public function createIngredients(RecipeRequest $request, Recipe $recipe): ?Collection
    {
        if (is_null($request->ingredients)) {
            return null;
        }

        return $recipe->ingredients()->createMany($request->ingredients);
    }

    /**
     * if request ingredient has ID, update ingredient, else create one.
     *
     * @param RecipeRequest $request
     * @param Recipe $recipe
     */
    public function editIngredients(RecipeRequest $request, Recipe $recipe): void
    {
        if (is_null($request->ingredients)) {
            return;
        }

        foreach ($request->ingredients as $ingredient) {
            if (array_key_exists('id', $ingredient)) {
                $recipe->ingredients()
                    ->where('id', $ingredient['id'])
                    ->update($ingredient);
                continue;
            }

            $recipe->ingredients()->create($ingredient);
        }
    }

    /**
     *
     * if not exist ingredients in database
     *  - do nothing and return
     *
     * if ingredient request is empty
     *  - delete all ingredients from database and return
     *
     * loop database ingredients and compare with request ingredients by id
     *  - if request ingredient does not exist
     *      - remove ingredient from database
     *
     * @param RecipeRequest $request
     * @param Recipe $recipe
     */
    public function deleteIngredients(RecipeRequest $request, Recipe $recipe): void
    {
        if (is_null($recipe->ingredients)) {
            return;
        }

        if (is_null($request->ingredients)) {
            $recipe->ingredients()
                ->delete();

            return;
        }

        foreach ($recipe->ingredients as $ingredient) {
            $isDeleted = true;

            foreach ($request->ingredients as $requestIngredient) {
                if (
                    array_key_exists('id', $requestIngredient)
                    && (int)$ingredient->id === (int)$requestIngredient['id']
                ) {
                    $isDeleted = false;
                    break;
                }
            }

            if ($isDeleted) {
                $recipe->ingredients()
                    ->where('id', $ingredient->id)
                    ->delete();
            }
        }
    }
}

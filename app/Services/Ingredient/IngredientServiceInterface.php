<?php


namespace App\Services\Ingredient;


use App\Http\Requests\RecipeRequest;
use App\Models\Recipe;
use Illuminate\Database\Eloquent\Collection;

interface IngredientServiceInterface
{
    /**
     * @param RecipeRequest $request
     * @param Recipe $recipe
     *
     * @return Collection|void
     */
    public function createIngredients(RecipeRequest $request, Recipe $recipe): ?Collection;

    /**
     * @param RecipeRequest $request
     * @param Recipe $recipe
     */
    public function editIngredients(RecipeRequest $request, Recipe $recipe): void;

    /**
     * @param RecipeRequest $request
     * @param Recipe $recipe
     */
    public function deleteIngredients(RecipeRequest $request, Recipe $recipe): void;
}

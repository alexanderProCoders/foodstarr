<?php

namespace App\Services\Localize;

use App\Services\Localize\Handlers\RequestLocalizeHandler;
use Illuminate\Http\Request;

class LocalizeService
{

    private RequestLocalizeHandler $requestLocalizeHandler;

    public function __construct(RequestLocalizeHandler $requestLocalizeHandler)
    {

        $this->requestLocalizeHandler = $requestLocalizeHandler;
    }

    public function localizeRequest(Request $request): void
    {
        $this->requestLocalizeHandler->handle($request);
    }
}

<?php

namespace App\Services\Localize\Handlers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class RequestLocalizeHandler
{

    public function handle(Request $request): void
    {
        $locale = $this->getSupportedLocale($request);

        if (!$locale) {
            abort(404);
        }
        App::setLocale($locale);
        $request->route()->forgetParameter('locale');
    }

    private function getSupportedLocale(Request $request): ?string
    {
        $locale = $request->route('locale');

        if (in_array($locale, config('locale.lang'), true)) {

            return $locale;
        }

        return null;
    }
}

<?php

namespace App\Http\Controllers;

use App\Services\Post\PostsService;
use Illuminate\Http\Request;

class PostController extends Controller
{

    private PostsService $postsService;

    public function __construct(PostsService $postsService)
    {
        $this->postsService = $postsService;
    }
    public function index(Request $request)
    {
        $posts = $this->postsService->searchPosts();

        return view('posts.index', compact('posts'));
    }

    public function show(Request $request)
    {
        $post = $this->postsService->showPostBySlug($request->slug);

        return view('posts.show', compact('post'));
    }
}

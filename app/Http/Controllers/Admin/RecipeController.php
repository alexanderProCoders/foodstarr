<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RecipeRequest;
use App\Services\Recipe\RecipeServiceInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class RecipeController extends Controller
{

    private RecipeServiceInterface $recipeService;

    public function __construct(RecipeServiceInterface $recipeService)
    {
        $this->recipeService = $recipeService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $recipes = $this->recipeService->getPaginatedRecipes();

        return view('admin.recipe.index', compact('recipes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('admin.recipe.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     *
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $recipe = $this->recipeService->getRecipeById($id);

        return view('admin.recipe.edit', compact('recipe'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RecipeRequest $request
     *
     * @return RedirectResponse
     */
    public function store(RecipeRequest $request)
    {
        $this->recipeService->createRecipe($request);

        return redirect()
            ->route('recipes.index')
            ->with('success', 'Recipe added successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RecipeRequest $request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function update(RecipeRequest $request, int $id): RedirectResponse
    {
        $this->recipeService->updateRecipe($request, $id);

        return redirect()
            ->route('recipes.index')
            ->with('success', 'Recipe updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $recipe = $this->recipeService->deleteRecipe($id);

        return redirect()
            ->route('recipes.index')
            ->with('success', "Recipe {$recipe->name} deleted successfully.");
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Services\Users\UsersService;
use Illuminate\Http\RedirectResponse;

class UserController extends Controller
{
    private UsersService $usersService;

    /**
     * @param UsersService $usersService
     */
    public function __construct(UsersService $usersService)
    {
        $this->usersService = $usersService;
    }

    /**
     * @param RegisterRequest $request
     * @return RedirectResponse
     */
    public function store(RegisterRequest $request): ?RedirectResponse
    {
        $data = $request->all();
        $user = $this->usersService->storeUser($data);

        if (is_null($user) === false) {
            session()->flash('success', 'Registration complete.');
            $this->usersService->login($user);

            return redirect()->route('admin.index');
        }

        abort(401, 'Authenticated');

        return null;
    }


    public function create()
    {
        return view('user.create');
    }

    public function loginForm()
    {
        return view('user.loginForm');
    }

    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function login(LoginRequest $request): RedirectResponse
    {
        $data = $request->getFormData();
       if ($this->usersService->loginByCredentials($data)) {
           session()->flash('success', 'You are logged in.');

           return redirect()->route('admin.index');
       }
       session()->flash('error', 'Incorrect email or password.');

       return redirect()->back()->with('error', 'Incorrect email or password.');
    }

    public function logout(): RedirectResponse
    {
        $this->usersService->logout();

        return redirect()->route('login.create');
    }
}

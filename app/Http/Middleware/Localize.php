<?php

namespace App\Http\Middleware;

use App\Services\Localize\LocalizeService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class Localize
{

    private LocalizeService $localizeService;

    public function __construct(LocalizeService $localizeService)
    {
        $this->localizeService = $localizeService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $this->localizeService->localizeRequest($request);

        return $next($request);
    }


}
